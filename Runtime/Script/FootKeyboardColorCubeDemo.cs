﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootKeyboardColorCubeDemo : MonoBehaviour
{
    public SerialPortToDigitalAndAnalogInput m_input;
    public Transform m_spawnPoint;
    public Camera m_camera;
    public Light m_light;

    [Header("Debug")]
    public Color m_currentColorLight;
    public Color m_wantedColorLight;
    public Color m_currentColorCamera;
    public Color m_wantedColorCamera;
    public float m_lerpFactor=1f;

    void Start()
    {
        m_input.SetAnalogListenerCountToMinimum(6);
        m_input.SetDigitalListenerCountToMinimum(2);

        m_input.ListenToDigitalChangeAt(0, SpawnBlock);
        m_input.ListenToDigitalChangeAt(1, SpawnCylinder);
        m_input.ListenToDigitalChangeAt(2, SpawnCapsule);
        m_input.ListenToDigitalChangeAt(3, SpawnSphere);
        m_input.ListenToDigitalChangeAt(4, SpawnBlock);
        m_input.ListenToDigitalChangeAt(5, SpawnCylinder);
        m_input.ListenToDigitalChangeAt(6, SpawnCapsule);
        m_input.ListenToDigitalChangeAt(7, SpawnSphere);
        m_input.ListenToDigitalChangeAt(8, SpawnBlock);
        m_input.ListenToDigitalChangeAt(9, SpawnCylinder);

        m_input.ListenToAnalogValueChangeAt(0, ChangeCameraRed);
        m_input.ListenToAnalogValueChangeAt(1, ChangeCameraGreen);
        m_input.ListenToAnalogValueChangeAt(2, ChangeCameraBlue);
        m_input.ListenToAnalogValueChangeAt(3, ChangeLightRed);
        m_input.ListenToAnalogValueChangeAt(4, ChangeLightGreen);
        m_input.ListenToAnalogValueChangeAt(5, ChangeLightBlue);
    }

    public void Update()
    {
        m_currentColorLight = Color.Lerp(m_currentColorLight, m_wantedColorLight, Time.deltaTime* m_lerpFactor);
        m_currentColorCamera = Color.Lerp(m_currentColorCamera,m_wantedColorCamera,  Time.deltaTime * m_lerpFactor);
        m_light.color = m_currentColorLight;
        m_camera.backgroundColor = m_currentColorCamera;
    }

    public void SpawnBlock(bool value)
    {

        CreatePrimitive(value, PrimitiveType.Cube);
    }
    public void SpawnCylinder(bool value)
    {
        CreatePrimitive(value, PrimitiveType.Cylinder);
    }
    public void SpawnCapsule(bool value)
    {
        CreatePrimitive(value, PrimitiveType.Capsule);
    }
    public void SpawnSphere(bool value)
    {
        CreatePrimitive(value, PrimitiveType.Sphere);
    }
    

    private void CreatePrimitive(bool value, PrimitiveType primitive)
    {
        if (!value) return;
        GameObject obj = GameObject.CreatePrimitive(primitive);
        SpawnParameters(obj);
    }

    private void SpawnParameters(GameObject obj)
    {
        obj.transform.position = m_spawnPoint.transform.position;
        Renderer render = obj.GetComponent<Renderer>();
        render.material.color = m_currentColorLight;
        obj.AddComponent<Rigidbody>();
    }

   

    public void ChangeCameraRed(int value)
    {
        m_wantedColorCamera.r=1f-(float)value/10f;
    }
    public void ChangeCameraGreen(int value)
    {
        m_wantedColorCamera.g = 1f - (float)value / 10f;
    }
    public void ChangeCameraBlue(int value)
    {
        m_wantedColorCamera.b = 1f - (float)value / 10f;
    }

    public void ChangeLightRed(int value)
    {
        m_wantedColorLight.r = 1f - (float)value / 10f;
    }                 
    public void ChangeLightGreen(int value)
    {

        m_wantedColorLight.g = 1f - (float)value / 10f;
    }                 
    public void ChangeLightBlue(int value)
    {
        m_wantedColorLight.b = 1f - (float)value / 10f;

    }
}
