﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;

public class SerialPortToDigitalAndAnalogInput : MonoBehaviour
{

    public List<ListenToDigital> m_listenToDigital= new List<ListenToDigital>();
    public List<ListenToAnalog> m_listenToAnalog= new List<ListenToAnalog>();
    [Header("Debug")]
    private string m_messageValideRegex= "#\\d*\\|\\d*";

    public string m_lastMessageReceived;



    public string m_lastValideMessageReceived;
    public char[] digits;
    public char[] analog;

    public void Reset()
    {
        for (int i = 0; i < 10; i++)
        {
            m_listenToDigital.Add(new ListenToDigital());

        }
        for (int i = 0; i < 6; i++)
        {
            m_listenToAnalog.Add(new ListenToAnalog());

        }
    }

    public void LastMessageReceived(ReadSerialPortTest.MessageToReceived arg0)
    {
        m_lastMessageReceived = arg0.m_message;
        if (Regex.Matches(m_lastMessageReceived, m_messageValideRegex,RegexOptions.IgnoreCase).Count>0) {
            m_lastValideMessageReceived = m_lastMessageReceived;
            string toConvert = m_lastMessageReceived;
            string[] tokens = toConvert.Replace("#", "").Split('|');
            digits = tokens[0].ToCharArray();
            analog = tokens[1].ToCharArray();



            for (int i = 0; i < digits.Length; i++)
            {
                if (i < m_listenToDigital.Count)
                {
                    m_listenToDigital[i].SetNewValue(ParseChar(digits[i]) == 0);
                }
            }
            for (int i = 0; i < analog.Length; i++)
            {
                if (i < m_listenToAnalog.Count)
                {
                    m_listenToAnalog[i].SetNewValue(ParseChar(analog[i]));
                }
            }

        }
    }

    private int ParseChar(char v)
    {
        switch (v)
        {
            case '1': return 1;
            case '2': return 2;
            case '3': return 3;
            case '4': return 4;
            case '5': return 5;
            case '6': return 6;
            case '7': return 7;
            case '8': return 8;
            case '9': return 9;
            default:
               return 0;
        }
    }

    public void ListenToDigitalChangeAt(int index, UnityAction<bool> actionToCall)
    {
        if (index < m_listenToDigital.Count)
            m_listenToDigital[index].m_onChange.AddListener(actionToCall);
    }

    public void ListenToAnalogValueChangeAt(int index, UnityAction<int> actionToCall)
    {
        if (index < m_listenToAnalog.Count)
            m_listenToAnalog[index].m_onAnalogChange.AddListener(actionToCall);
    }
    public void ListenToAnalogChangeAt(int index, UnityAction<bool> actionToCall)
    {
        if (index < m_listenToAnalog.Count)
            m_listenToAnalog[index].m_onDigitalChange.AddListener(actionToCall);
    }


    public void SetDigitalListenerCountToMinimum(int v)
    {
        int difference = v - m_listenToDigital.Count;
        if (difference > 0)
            for (int i = 0; i < difference; i++)
            {
                m_listenToDigital.Add(new ListenToDigital());

            }

    }
    public void SetAnalogListenerCountToMinimum(int v)
    {
        int difference = v - m_listenToAnalog.Count;
        if (difference > 0)
            for (int i = 0; i < difference; i++)
            {
                m_listenToAnalog.Add(new ListenToAnalog());

            }
    }




    [Serializable]
    public class ListenToDigital
    {
        public BoolChangeEvent m_onChange;
        public bool m_value;

        public void SetNewValue(bool value)
        {
            if (m_value != value) {
                m_value = value;
                m_onChange.Invoke(m_value);
            }
        }
    }
    [Serializable]
    public class ListenToAnalog
    {
        public FloatChangeEvent m_onAnalogChange;
        public int m_threshold =3;
        public BoolChangeEvent m_onDigitalChange;
        public int m_analogValue;
        public bool m_boolValue;

        public void SetNewValue(int value)
        {
            bool boolValue = value > m_threshold;
            if (m_boolValue != boolValue)
            {
                m_boolValue = boolValue;
                m_onDigitalChange.Invoke(m_boolValue);
            }
            if (m_analogValue != value) {
                m_analogValue = value;
                m_onAnalogChange.Invoke(m_analogValue);
            }

        }
    }

    [Serializable]
    public class BoolChangeEvent : UnityEvent<bool> { }
    [Serializable]
    public class FloatChangeEvent : UnityEvent<int> { }
}
